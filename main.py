
# compatible_mode = 0 if fasttext works
# compatible_mode = 1 if fasttext does not work

from flask import Flask, render_template, jsonify
from flask_restful import reqparse
from flask import render_template
from flask import request
from flask import Response
import requests
import reprocessing as rp
import os.path
compatible_mode = 0



word_vector = 'model1.vec'
trained_model = 'test.model2.bin'
if (os.path.isfile(word_vector) and os.path.isfile(trained_model)):
    print('trained model exists, all good to go')
else:
  rp.train_word_vector()
  rp.train()
  if ( not os.path.isfile(word_vector) or not os.path.isfile(trained_model)):
      compatible_mode = 1
      print("fasttext cannot run, switch to compatible mode!")
  else:
       print('start normally, everything is fine')


app = Flask(__name__)

url = "https://api.dialogflow.com/v1/query?v=20150910"
url_intents = "https://api.dialogflow.com/v1/intents?v=20150910"
data = {}

def get_post_args(arg_names, arg_types):
    parser = reqparse.RequestParser()
    for i in range(len(arg_names)):
        parser.add_argument(arg_names[i], type=arg_types[i])
    return parser.parse_args()

@app.route('/')
def index():
    return render_template('index.html')

@app.route("/train", methods=['POST'])
def train():
    text = request.form.get('text')
    sentence = rp.content_clean(str(text))
    key_words = rp.key_extraction(sentence)
    if (compatible_mode == 1):
        topics = ''
    else:
        topics = rp.predict(sentence, 2)
    data["key_words"] = key_words
    data["potential_topics"] = topics
    return jsonify(data)

@app.route('/chat', methods=['POST'])
def chat():
    header = {"Content-Type": "application/json","Authorization":"Bearer 2b3c7808b6e94b86a9dc8f40a5b3d576"}
    query = request.form.get('data')
    query = rp.sentence_process(query, compatible_mode)
    body = {"lang": "en",
    "query": query,
    "sessionId": "12345"}
    print(body)
    response = requests.post(url, json=body, headers=header )
    return Response(response)

@app.route('/intents', methods=['POST'])
def intents():
    header = {"Content-Type": "application/json","Authorization":"Bearer ca06c8da9a294fd98aed486c5ab759a9"}
    name = request.form.get('name')
    question = request.form.get('question')
    response = request.form.get('response')
    rp.add_new_train(question,name,response,'test.txt','pre_train.csv')
    print(name,question,response)
    body = {
  "contexts": [

  ],
  "events": [],
  "name": name,
  "priority": 500000,
  "responses": [
    {
      "action": "add.list",
      "messages": [
        {
          "speech": response,
          "type": 0
        }
      ]
    }
  ],
  "templates": [
  ],
  "userSays": [

    {
      "count": 0,
      "data": [
        {
          "text": question
        }
      ]
    }
  ]
}
    response = requests.post(url_intents, json=body, headers=header )
    return Response(response)

@app.route('/intents_train', methods=['POST'])
def intents_train():
    header = {"Content-Type": "application/json","Authorization":"Bearer ca06c8da9a294fd98aed486c5ab759a9"}
    name = request.form.get('name')
    question = request.form.get('question')
    response = request.form.get('response')
    if (compatible_mode == 0):
        print ('train_start')
        rp.add_new_train(question,name,response,'test.txt','pre_train.csv')
        rp.train()
    print(name,question,response)
    body = {
  "contexts": [

  ],
  "events": [],
  "name": name,
  "priority": 500000,
  "responses": [
    {
      "action": "add.list",
      "messages": [
        {
          "speech": response,
          "type": 0
        }
      ]
    }
  ],
  "templates": [
  ],
  "userSays": [

    {
      "count": 0,
      "data": [
        {
          "text": question
        }
      ]
    }
  ]
}
    response = requests.post(url_intents, json=body, headers=header )
    return Response(response)

if __name__ == '__main__':
    app.run()
