import reprocessing as rp


import fasttext
from rake_nltk import Rake

def train_word_vector():
    model = fasttext.cbow('new_train.csv','model', dim=300, word_ngrams=2)




def train():
    cf = rp.fasttext.supervised("test.txt", "test.model1",lr=0.2, dim=300, epoch=50,
                         label_prefix="__label__",pretrained_vectors="model.vec"
                         )

# predict question topic using fasttext
def predict(sentence, number):
    cf = rp.fasttext.load_model('test.model1.bin')
    ans = cf.predict([sentence],k=number)
    ans1 = ans[0]
    final = [x.replace('__label__','') for x in ans1]
    return  final

def predict_with_prob(sentence):
    cf = rp.fasttext.load_model('test.model1.bin')
    ans = cf.predict_proba([sentence])
    ans1 = ans[0][0]
    return  ans1



def sentence_process(sentence):
    temp_topics = predict_with_prob(sentence)
    if (temp_topics[1] > 0.3):
        protential_topic =  temp_topics[0]
        protential_topic = protential_topic.replace('__label__','')
        protential_topic = protential_topic.replace('_', ' ')    
        final_sentence = sentence + " " + protential_topic
    else:
        final_sentence = sentence
    return final_sentence

    




# train_word_vector()
# train()
sentence = "tell me more about comp9900"
print(predict(sentence, 2))
print(predict_with_prob(sentence))
a = sentence_process(sentence)
print (a)

