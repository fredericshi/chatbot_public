README

run     ./install.sh  only if it is the first time runing the application

run     python main.py to start the application  if has run install.sh before


file    install.sh is the bash script for install all needed libraries

file    reprocessing.py is the python script for dealing with data clean, key extraction, intent extraction, training data reparation

file    main.py is the python script for run the application

file    model1.vec is the word vector presentation of fasttext, which is what we need for training. training without that should be fine, but it improve    s the accurucy. 
 
file    model1.bin is the binary model built by fasttext, we do not need that
 
file    new_train.csv is the csv training file contain all wiki QA
 
file    test.txt  is the training file for fasttext classification with __label__ marked as intent or topics
 
file    testing_file.txt is the testing file for fasttext classification
 
file    train0.csv train1.csv train2.csv is the split file of pre_train.csv in case that there is a limit on intents number

file    ggg.txt is the temporay file for runing fasttext prediction

folder  course_info contains all training json files which can be uploaded to dialogflow directly as well as python script to grasp information from handbook.

file    v0.1.0.zip is the fasttext install zip file 

