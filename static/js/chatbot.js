const accessToken = "e1d7492dc6ba4ff9909f3fd6f7d645d7",
    baseurl = "https://api.dialogflow.com/v1/",
    message_recording = "Recording...",
    speech = $("#speech");



$(document).ready(function() {
  $('#speech').keyup(function(e){
    if(e.which == 13) {// ENTER key presse
  var text = speech.val();
      speech.val('');

  $("#chat-answer").append("<li class=\"self\"><div class=\"msg\"> " + text + "</div></li>")
  console.log(text);
  if (text=="mode change") {
    $('#chatcommand').css('display', 'block');
            $('#chat-bubble').css('display', 'none');
            $('#chat-footer').css('display', 'none');
  } else {
      $.ajax({
          type: "POST",
          url: '/chat',
          data: {'data': text},
          success: function (data) {
              console.log(data);
              var answer = JSON.parse(data).result.fulfillment.speech;
              console.log(answer);
              $("#chat-answer").append("<li class=\"other\"><div class=\"msg\"> " + answer + "</div></li>")
          },
          error: function () {
              console.log("error");
          }
      });
  }
    }
  });


  $("#intent-btn").click(function(){
     intent_name = $("#intent_name").val();
     intent_question = $("#intent_question").val();
     intent_response = $("#intent_response").val();
     $.ajax({
          type: "POST",
          url: '/intents',
          data: {'name': intent_name,
          'question':intent_question,
          'response':intent_response},
          success: function (data) {
              var answer = JSON.parse(data);
              if (answer['status']['code'] == '200') {
                  alert("Upload Sucess, please refresh page");
                  console.log(answer);
              } else {
                  alert("Intent already existss");
                  console.log(answer);
              }
          },
          error: function () {
              alert("Intent already existss");
              console.log("error");
          }
      });
  });


});

