/*!
 * jQuery CLI
 * Simulating a command line interface with jQuery
 *
 * @version : 1.0.0
 * @author : Paulo Nunes (http://syndicatefx.com)
 * @demo : https://codepen.io/syndicatefx/pen/jPxXpz
 * @license: MIT
 */

/*!*
 * jQuery Text Typer plugin
 * https://github.com/gr8pathik/jquery-texttyper
*/
(function(e){"use strict";e.fn.textTyper=function(t){var n={typingClass:"typing",beforeAnimation:function(){},afterAnimation:function(){},speed:10,nextLineDelay:400,startsFrom:0,repeatAnimation:false,repeatDelay:4e3,repeatTimes:1,cursorHtml:'<span class="cursor">|</span>'},r=e.extend({},n,t);this.each(function(){var t=e(this),n=1,i="typingCursor";var s=t,o=s.length,u=[];while(o--){u[o]=e.trim(e(s[o]).html());e(s[o]).html("")}t.init=function(e){var n=r.beforeAnimation;if(n)n();t.animate(0)};t.animate=function(o){var a=s[o],f=r.typingClass,l=r.startsFrom;e(a).addClass(f);var c=setInterval(function(){var f=r.cursorHtml;f=e("<div>").append(e(f).addClass(i)).html();e(a).html(u[o].substr(0,l)+f);l++;if(u[o].length<l){clearInterval(c);o++;if(s[o]){setTimeout(function(){e(a).html(u[o-1]);t.animate(o)},r.nextLineDelay)}else{e(a).find("."+i).remove();if(r.repeatAnimation&&(r.repeatTimes==0||n<r.repeatTimes)){setTimeout(function(){t.animate(0);n++},r.repeatDelay)}else{var h=r.afterAnimation;if(h)h()}}}},r.speed)};t.init()});return this}})(jQuery)


// Let's do it!!
$(document).ready(function() {

  $('#chatcommand .command').hide();
  $('#chatcommand input[type="text"]').focus();
  $('#chatcommand #home').addClass('open');
  $('#chatcommand #home').textTyper({
      speed: 20,
      afterAnimation: function () {
          $('#chatcommand .command').fadeIn();
          $('#chatcommand input[type="text"]').focus();
          $('#chatcommand input[type="text"]').val('');
      }

      });

// Command Input------------------------------

  $('#chatcommand input[type="text"]').keyup(function(e){

    if(e.which == 13) {// ENTER key presse
        var text = $('#chatcommand input[type="text"]').val();
        $('#chatcommand input[type="text"]').val('');
        $("#command-result").append("<span class='command'>Safari:/$<input readonly=\"readonly\" type=\"\" value='" + text + "'></span>");
        console.log(text);
        if (text != '') {
            if (text == "mode change") {
                $('#chatcommand').css('display', 'none');
                $('#chat-bubble').css('display', 'block');
                $('#chat-footer').css('display', 'block');
            } else {
                $('#chatcommand > .command').hide();
                $.ajax({
                    type: "POST",
                    url: '/chat',
                    data: {'data': text},
                    success: function (data) {
                        console.log(data);
                        var answer = JSON.parse(data);

                        if (answer["status"]["code"] == '200') {
                            var s_answer = answer.result.fulfillment.speech;


                            $("#command-result").append("<p class='open'>" + s_answer + "</p>");
                        } else {
                            var e_answer = answer["status"]["errorDetails"];
                            $("#command-result").append("<p class='open'>" + e_answer + "</p>");
                        }
                        $("#command-result p:last-child").textTyper({
                            speed: 20,
                            afterAnimation: function () {
                                $('.command').fadeIn();
                                $('#chatcommand > .command').show();
                                $('#chatcommand > input[type="text"]').focus();
                                $('#chatcommand > input[type="text"]').val('');
                            }
                        });

                    },
                    error: function () {
                        console.log("error");
                    }
                });


            }// end if ENTER key pressed
        }
    }
  });// end keyup function

// End Command Input-----------------------------

});