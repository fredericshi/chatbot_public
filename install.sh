#!/bin/bash

if [ -d fastText ]
then
    rm -rf fastText
fi

pip install nltk
pip install numpy
pip install pandas
echo 'import nltk \n nltk.download()' > nltk_data.py
python nltk_data.py

unzip v0.1.0.zip
mv fastText-0.1.0 fastText
cd fastText
make

