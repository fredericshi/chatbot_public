# here  marked as 300d.vec
#
#
# nltk applied rake  api   install by : pip install rake-nltk
# nltk                     install by : pip install nltk
# first time installing nltk, need to download stopwords corpus etc. therefore, call the function nltk_data_download.
#
# pandas                   install by : pip install pandas
#
#
# each time the train takes about 10 minutes, pay attention to that
# fasttext word vector:
# @article{bojanowski2017enriching,
#   title={Enriching Word Vectors with Subword Information},
#   author={Bojanowski, Piotr and Grave, Edouard and Joulin, Armand and Mikolov, Tomas},
#   journal={Transactions of the Association for Computational Linguistics},
#   volume={5},
#   year={2017},
#   issn={2307-387X},
#   pages={135--146}
# }

# fasttext text classification:
# @InProceedings{joulin2017bag,
#   title={Bag of Tricks for Efficient Text Classification},
#   author={Joulin, Armand and Grave, Edouard and Bojanowski, Piotr and Mikolov, Tomas},
#   booktitle={Proceedings of the 15th Conference of the European Chapter of the Association for Computational Linguistics: Volume 2, Short Papers},
#   month={April},
#   year={2017},
#   publisher={Association for Computational Linguistics},
#   pages={427--431},
# }
#
#
#

# reprocessing
from rake_nltk import Rake
import pandas as pd
from nltk.tokenize import word_tokenize

import io
# file_name = "300d.vec"
import csv
import nltk
import subprocess


def nltk_data_download():
    nltk.download()




# key words extraction function based on rapid automatic keyword extraction
def key_extraction(string):
    r = Rake()
    r.extract_keywords_from_text(string)
    output =  r.get_ranked_phrases_with_scores()
    return output

# change all word to lowercase
def content_clean(string):
    tokens = word_tokenize(string)
    words_list = [word.lower() for word in tokens if word.isalpha()]
    return  ' '.join(words_list)


def obtain_fasttext_format(string):
    label = '__label__'
    list = string.split('\t')
    title = label + list[0]
    question = content_clean(list[1])
    ans = list[2]
    if (list[2] == 'NULL'):
        return 0
    else:
        fasttext = title + ' ' + question + '\n'
        train = list[0] + '\t' + question + '\t' + ans + '\n'
        return fasttext, train




# QA training data preparation
def qa_pair_prepare(input_file, file_to_com, train_file_to_com ):
    file1 = open (file_to_com,'a')
    file2 = open (train_file_to_com, 'a')
    label = "__label__"
    sep = "\t"

    data = pd.read_csv(input_file, sep='\t', header=None)
    data1 = data.drop([3,4,5],axis=1)
    data2 = data1.drop_duplicates()
    data3 = data2.reset_index()
    length = data3.shape[0]
    start = 1
    while( start < length):
        if ( str(data3.loc[start,2]) != 'nan' and str(data3.loc[start,2]) !=' ' ):
            fast = label + str(data3.loc[start,0]) +  ' ' + content_clean(str(data3.loc[start,1])) + '\n'
            train = str(data3.loc[start,0]) + sep + content_clean(str(data3.loc[start,1])) + sep + str(data3.loc[start, 2]) + '\n'

            file1.write(fast)
            file2.write(train)
        start = start + 1


    file1.close()
    file2.close()



# predict sentence ans if there is only one answer in model, not needed yet
def predict_ans(sentence):
    data = pd.read_csv('pre_train.csv',sep='\t',header=None)
    hash_table = {}
    start = 0

    while(start < data.shape[0]):
        hash_table[data.loc[start,0]] = data.loc[start,2]
        start = start + 1
 
    topic = predict(sentence,1)[0]
    # number = len(hash_table[topic])
    ans = hash_table[data.loc[1,0]]
    print(data.loc[1,0])
    print(hash_table[data.loc[1,0]])
    print(hash_table['Glacier cave'])
    if (topic == data.loc[1,0]):
        print('good')
    prob = 1.0



    return  ans, prob


def train_file_prepare(file_in, file_out):
    data = pd.read_csv(file_in,sep='\t',header=None, encoding='utf-8')
    start = 1
    target = 0
    former = 0
    data.insert(0,'code','1')
    data.reset_index()
    data.loc[target,'code'] = target
    
    while(start  < data.shape[0]):
        if (data.loc[start, 0] == data.loc[former, 0]):
            # print('good', target, start, data.loc[start, 0], data.loc[target, 0])
            data.loc[start,'code'] = target
        else:
            target += 1
            former = start
            data.loc[start,'code'] = target
        start += 1

    data.to_csv(path_or_buf=file_out, header=['code', 'entity','question','answer'],index=False)




def add_new_train(question, topic, ans, file, train):
    file1 = open(file, 'a')
    file2 = open(train, 'a')
    sep = '\t'
    question = content_clean(question)
    topic1  = "__label__" +  topic.replace(' ', '_')
    text = topic1 + ' ' +  question + '\n'
    train_text = topic.replace(' ','_') + sep + question + sep + ans + '\n'
    file1.write(text)
    file2.write(train_text)



def wiki_prepare(input_file, output_file, train_file):
    data0 = pd.read_csv(input_file, sep='\t', header=None)
    data = data0.drop([0], axis=0)
    data1 = data.drop([0,2,4,5,6],axis=1)
    data2 = data1.drop_duplicates()
    # print (data)
    data3 = data2.reset_index()
    length = data2.shape[0]
    label = "__label__"

    with open(output_file, 'w') as f:
        a = 0
        while(a < length):
            string = data3.loc[a,3]
            string = string.replace(" ", "_")

            f.write(label + string + " " + content_clean(data3.loc[a,1]) + "\n")
            a = a + 1

    with open(train_file, 'w', newline='') as f:
        # writer = csv.writer(f, dialect='excel')
        for a in data2.index.tolist():
            string_split = "\t"
            string = data.loc[a, 3] + string_split + content_clean(data.loc[a, 1]) + \
                     string_split + data.loc[a, 5]
            f.write(string + "\n")


def train_word_vector():
    shell_script = 'fastText/./fasttext skipgram -input pre_train.csv -output model1 -epoch 50 -lr 0.01 -dim 300 '
    subprocess.call(shell_script,shell=True)
# train model for fasttext using both corpus and 300d word vector
def train():
    shell_script = 'fastText/./fasttext supervised -input test.txt -output test.model2 -wordNgrams 2 -dim 300 -epoch 25 -pretrainedVectors model1.vec -lr 1.0 -loss hs'
    subprocess.call(shell_script, shell=True)

# predict question topic using fasttext
def predict(sentence, number):
    with open('ggg.txt','w') as f:
        f.write(sentence)
    shell_script = 'fastText/./fasttext predict test.model2.bin ggg.txt ' + str(number)
    ans = subprocess.check_output(shell_script, shell=True)
    ans_temp = ans.strip('\n')
    ans_temp = ans_temp.split('__label__')
    ans_temp.pop(0)
    ans_temp = [x.replace('__label__', '').replace('_', ' ') for x in ans_temp]
    return  ans_temp

def predict_with_prob(sentence):
    with open('ggg.txt','w') as f:
        f.write(sentence)
    shell_script = 'fastText/./fasttext predict-prob test.model2.bin ggg.txt'
    ans = subprocess.check_output(shell_script, shell=True)
    print(ans)
    if (ans != ''):
        ans = ans.strip('\n')
        ans = ans.replace('__label__', '')
        ans = ans.split(' ')
        ans[1] = float(ans[1])
        return  ans
    else:
        list1 = []
        a = 0.1
        list1.append(sentence)
        list1.append(a)
        return list1

def sentence_process(sentence, mode):
    if (len(sentence) > 20 and mode == 0):
        temp_topics = predict_with_prob(sentence)
        if (temp_topics[1] > 0.4 ):
            protential_topic =  temp_topics[0]
            protential_topic = protential_topic.replace('__label__','')
            protential_topic = protential_topic.replace('_', ' ')    
            final_sentence = sentence + " " + protential_topic
            return final_sentence
    
    return sentence


def testing():
    while(1):
        a = int(input(("1. prepare data \n2. train\n3. add_sentence\n4. test_sentence\n5. quit\nchoice: " )))
        if (a == 1):
            wiki_prepare('WikiQA.tsv', 'test.txt', 'pre_train.csv')
            qa_pair_prepare('qa_pair.csv', 'test.txt', 'pre_train.csv')
            train_file_prepare('pre_train.csv','new_train.csv')
            print('finished\n')
        if (a == 2):
            train()
        if (a == 3):
            sentence = content_clean(str(input("enter your question : ")))
            print('key words are:')
            output = key_extraction(sentence)
            print(output)
            print('protential topics are: ')
            print(predict(sentence, 2))
            topic = str(input("enter the topic :"))
            print()
            ans = str(input('enter answer '))
            add_new_train(sentence,topic, ans, 'test.txt', 'pre_train.csv')
            train_file_prepare('pre_train.csv','new_train.csv')
            train()


        if (a== 4):
            sentence = content_clean(str(input("enter your question : ")))
            # sentence = 'mac is worse than win'
            print('key words are: ')
            print(key_extraction(sentence))
            print()
            print('protential topics are: ')
            topic1 = predict(sentence, 4)
            print(topic1)
        if (a == 5):
            break


