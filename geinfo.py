import requests
import re
import json

URL = "http://legacy.handbook.unsw.edu.au/postgraduate/courses/2018/COMP9024.html"

COURSE_URL_PRE = "http://legacy.handbook.unsw.edu.au/vbook2018/brCoursesByAtoZ.jsp?StudyLevel=Postgraduate&descr="

char = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

#Course code
code=[]
#Course name
name=[]
#Course Info
info=[]

for i in range(len(char)):
    COURSE_URL=COURSE_URL_PRE+char[i]
    print(COURSE_URL)
    r = requests.get(COURSE_URL, stream=True)

    for line in r.iter_lines():

        if line:
            l=str(line)
            m = re.match(".*<TD.*>([A-Z]+[0-9]+)<.*",l)
            m1 = re.match(".*<TD.*href=\"(http.*html)\">(.*)</A.*",l)
            if m:
                code.append(m.group(1))
            if m1:
                info.append(m1.group(1))
                n = re.match("(.*)\(.*",m1.group(2).rstrip().strip('\/()'))
                if n:
                    name.append(n.group(1))
                else:
                    name.append(m1.group(2))

#course list
print(len(code))
print(len(name))
print(len(info))

print(code[500])
print(name[500])
print(info[500])

entries = []

d_name = dict()

for i in range(len(code)):
    d2 = dict()
    d2["value"]=info[i]
    d2["synonyms"]=[name[i],code[i]]
    entries.append(d2)
    d_name[code[i]]=name[i]

#print(entries[1])

fjson = open("courses.json")
entity = json.load(fjson)
entity["entries"] = entries
#print(entity)

outfile = open("courses_1.json","w")
json.dump(entity,outfile)

entries_name=[]
for i in range(len(code)):
    d1 = dict()
    d1["value"]=name[i]
    d1["synonyms"]=[code[i]]
    entries_name.append(d1)

fname = open("Course_name.json")
entity_name = json.load(fname)
entity_name["entries"] = entries_name

outfile_1 = open("Course_name_1.json","w")
json.dump(entity_name,outfile_1)

#CLIENT_ACCESS_TOKEN = '<>'

f = open("text.txt")
content = f.readlines()
content = [x.strip() for x in content]

for line in content:
    print(line)

print(info[0])

d = dict()

for URL in info:
    m_course = re.match(".*/2018/(.*).html", URL)
    #print(m_course.group(1))

    d[m_course.group(1)]=dict()
    description_sw = 0
    r = requests.get(URL, stream=True)
    for line in r.iter_lines():
        if line:
            # print(line)
            m_school = re.match(".*School.*<a href=.*\">(.*)</a.*Course.*", str(line))
            m_description = re.match(".*<h2>Description</h2>(.*)", str(line))
            m_div = re.match(".*<div>(.*)</div>.*", str(line))
            m_prerequisite = re.match(".*Prerequisite:(.*)</p><.*", str(line))
            m_faculty = re.match(".*Faculty:.*html\">(.*)</a.*", str(line))
            m_courseoutline = re.match(".*Course Outline:.*<a.*>(.*)</a>.*", str(line))
            m_campus = re.match(".*Campus:.*&nbsp;(.*)</p.*", str(line))
            m_timetable = re.match(".*Further Information:.*<a href=\"(.*)\">See Class Timetable.*", str(line))
            m_unitsofcredit = re.match(".*Units of Credit:.*&nbsp;(.*)</p>", str(line))
            m_eftsl = re.match(".*EFTSL:.*&nbsp;(.*)&nbsp.*", str(line))
            m_career = re.match(".*Career:.*&nbsp;(.*)</p>.*", str(line))

            # School
            if m_school:
                # print(line)
                #print("School: ", m_school.group(1))
                d[m_course.group(1)]["School"] = m_school.group(1)
            # Description
            if m_description:
                description_sw = 1
            if (description_sw == 1 and m_div):
                # print(line)
                #print("Description: ", m_div.group(1))
                d[m_course.group(1)]["Description"] = m_div.group(1)
                description_sw = 0
            # Prerequisite
            if m_prerequisite:
                # print(line)
                #print("Prerequisite: ", m_prerequisite.group(1).rstrip().lstrip())
                d[m_course.group(1)]["Prerequisite"] = m_prerequisite.group(1).rstrip().lstrip()
            # Faculty
            if m_faculty:
                #print("Faculty: ", m_faculty.group(1))
                d[m_course.group(1)]["Faculty"] = m_faculty.group(1)
            if m_courseoutline:
                #print("Course outline: ", m_courseoutline.group(1))
                d[m_course.group(1)]["Course_outline"] = m_courseoutline.group(1)
            if m_campus:
                #print("Campus: ", m_campus.group(1))
                d[m_course.group(1)]["Campus"] = m_campus.group(1)
            # Timetable
            if m_timetable:
                #print("Timetable: ", m_timetable.group(1))
                d[m_course.group(1)]["Timetable"] = m_timetable.group(1)
            if m_unitsofcredit:
                #print("Units of Credits: ", m_unitsofcredit.group(1))
                d[m_course.group(1)]["Credits"] = m_unitsofcredit.group(1)
            if m_eftsl:
                #print("EFTSL: ", m_eftsl.group(1))
                d[m_course.group(1)]["EFTSL"] = m_eftsl.group(1)
            if m_career:
                #print("Career: ", m_career.group(1))
                d[m_course.group(1)]["Career"] = m_career.group(1)

print(len(d))

entries_faculty = []
entries_school = []
entries_course_outline = []
entries_campus = []
entries_career = []
entries_credits = []
entries_eftsl = []
entries_prerequisite = []
entries_timetable = []
entries_description = []

for e in d:
    if "Faculty" in d[e]:
        d_faculty = dict()
        d_faculty["value"]=d[e]["Faculty"]
        d_faculty["synonyms"]=[e]
        entries_faculty.append(d_faculty)
    if "School" in d[e]:
        d_school = dict()
        d_school["value"]=d[e]["School"]
        d_school["synonyms"]=[e]
        entries_school.append(d_school)
    if "Course_outline" in d[e]:
        d_course_outline = dict()
        d_course_outline["value"] = d[e]["Course_outline"]
        d_course_outline["synonyms"]=[e]
        entries_course_outline.append(d_course_outline)
    if "Campus" in d[e]:
        d_campus = dict()
        d_campus["value"]=d[e]["Campus"]
        d_campus["synonyms"]=[e]
        entries_campus.append(d_campus)
    if "Career" in d[e]:
        d_career = dict()
        d_career["value"]=d[e]["Career"]
        d_career["synonyms"]=[e]
        entries_career.append(d_career)
    if "Credits" in d[e]:
        d_credits = dict()
        d_credits["value"]=d[e]["Credits"]
        d_credits["synonyms"]=[e]
        entries_credits.append(d_credits)
    if "EFTSL" in d[e]:
        d_eftsl = dict()
        d_eftsl["value"]=d[e]["EFTSL"]
        d_eftsl["synonyms"]=[e]
        entries_eftsl.append(d_eftsl)
    if "Prerequisite" in d[e]:
        d_prerequisite = dict()
        d_prerequisite["value"]=d[e]["Prerequisite"]
        d_prerequisite["synonyms"]=[e]
        entries_prerequisite.append(d_prerequisite)
    if "Timetable" in d[e]:
        d_timetable = dict()
        d_timetable["value"]=d[e]["Timetable"]
        d_timetable["synonyms"]=[e]
        entries_timetable.append(d_timetable)
    if "Description" in d[e]:
        d_description = dict()
        d_description["value"]=d[e]["Description"]
        d_description["synonyms"]=[e]
        entries_description.append(d_description)



f_faculty = open("course_faculty.json")
entity_faculty = json.load(f_faculty)
entity_faculty["entries"] = entries_faculty
#print(entity)

faculty_outfile = open("course_faculty_1.json","w")
json.dump(entity_faculty,faculty_outfile)

f_school = open("course_school.json")
entity_school = json.load(f_school)
entity_school["entries"] = entries_school
#print(entity)

school_outfile = open("course_school_1.json","w")
json.dump(entity_school,school_outfile)

f_coutline = open("course_outline.json")
entity_coutline = json.load(f_coutline)
entity_coutline["entries"] = entries_course_outline
#print(entity)

coutline_outfile = open("course_outline_1.json","w")
json.dump(entity_coutline,coutline_outfile)

f_campus = open("course_campus.json")
entity_campus = json.load(f_campus)
entity_campus["entries"] = entries_campus
#print(entity)

campus_outfile = open("course_campus_1.json","w")
json.dump(entity_campus,campus_outfile)

f_career = open("course_career.json")
entity_career = json.load(f_career)
entity_career["entries"] = entries_career
#print(entity)

career_outfile = open("course_career_1.json","w")
json.dump(entity_career,career_outfile)

f_credits = open("units_of_credits.json")
entity_credits = json.load(f_credits)
entity_credits["entries"] = entries_credits
#print(entity)

credits_outfile = open("units_of_credits_1.json","w")
json.dump(entity_credits,credits_outfile)

#eftsl

f_eftsl = open("course_eftsl.json")
entity_eftsl = json.load(f_eftsl)
entity_eftsl["entries"] = entries_eftsl
#print(entity)

eftsl_outfile = open("course_eftsl_1.json","w")
json.dump(entity_eftsl,eftsl_outfile)

#prerequisite

f_prerequisite = open("course_prerequisite.json")
entity_prerequisite = json.load(f_prerequisite)
entity_prerequisite["entries"] = entries_prerequisite
#print(entity)

prerequisite_outfile = open("course_prerequisite_1.json","w")
json.dump(entity_prerequisite,prerequisite_outfile)

#Description


f_description = open("course_description.json")
entity_description = json.load(f_description)
entity_description["entries"] = entries_description
#print(entity)

description_outfile = open("course_description_1.json","w")
json.dump(entity_description,description_outfile)