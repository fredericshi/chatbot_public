import reprocessing as rp
import subprocess
import time
def train(number):
    shell_script = 'fastText/./fasttext supervised -input test.txt -output test.model2 -wordNgrams 2 -dim 300 -epoch ' + str(number) +  ' -pretrainedVectors model1.vec -lr 1.0 -loss hs'
    subprocess.call(shell_script, shell=True)

a = [9.588040113449097, 9.433848857879639, 9.75616192817688, 10.020850896835327, 11.51655101776123, 11.171983003616333, 11.387063026428223, 11.63336706161499, 12.14731502532959, 12.067105054855347, 11.834634065628052]

start = time.time()
train(200)
end = time.time()
print(end - start)

